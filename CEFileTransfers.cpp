#pragma once
#include "stdafx.h"
#include "CEFileTransfers.h"
#include <iostream>
#include <sstream>
#include <windows.h>

#define fARRAYSIZE(x) (sizeof(x)/sizeof(x[0]))


/*


int GetFileFromDevice(CString sourcefile,CString destfile,int flag);
int PutFileOnDevice(CString sourcefile,CString destfile,int flag);


*/
HRESULT TryRapiConnect(DWORD dwTimeOut) {
 HRESULT      hr = E_FAIL;
    RAPIINIT     riCopy;
    bool         fInitialized = false;

    ZeroMemory(&riCopy, sizeof(riCopy));
    riCopy.cbSize = sizeof(riCopy);

    hr = CeRapiInitEx(&riCopy);
    if (SUCCEEDED(hr))
    {
        DWORD dwRapiInit = 0;
        fInitialized = true;

        dwRapiInit = WaitForSingleObject(
                    riCopy.heRapiInit,
                    dwTimeOut);
        if (WAIT_OBJECT_0 == dwRapiInit)
        {
            //  heRapiInit signaled:
            // set return error code to return value of RAPI Init function
            hr = riCopy.hrRapiInit;  
        }
        else if (WAIT_TIMEOUT == dwRapiInit)
        {
            // timed out: device is probably not connected
            // or not responding
            hr = HRESULT_FROM_WIN32(ERROR_TIMEOUT);
        }
        else
        {
            // WaitForSingleObject failed
            hr = HRESULT_FROM_WIN32(GetLastError());
        }
    }

   if (fInitialized && FAILED(hr))
   {
       // CeRapiUninit();
   }
    return hr;
	}

int GetFileFromDevice(CString source,CString dest,int flag) {

TCHAR wszSrcFile[MAX_PATH];
WCHAR tszDestFile[MAX_PATH];
BYTE  Buffer[4096];

CStringA sourcefile(source);
CStringA destfile(dest);
CString logtmp;

    HANDLE hSrc, hDest, hFind;
    CE_FIND_DATA wfd;
    HRESULT hRapiResult;
    HRESULT hr;
    DWORD dwAttr, dwNumRead, dwNumWritten;    
    int nResult;

  
    nResult = MultiByteToWideChar(
                    CP_ACP,    
                    MB_PRECOMPOSED,
                    sourcefile,
                   sourcefile.GetLength() +1,
                    wszSrcFile,
                    fARRAYSIZE(wszSrcFile));
    if(0 == nResult)
    {
        return 1;
    }


    nResult = MultiByteToWideChar(
                    CP_ACP,    
                    MB_PRECOMPOSED,
                    destfile,
                    destfile.GetLength() +1,
                    tszDestFile,
                    fARRAYSIZE(tszDestFile));
    if(0 == nResult)
    {
        return 1;
    }
    HRESULT rapicon = TryRapiConnect(500);
	if (rapicon != 0)
		return 1;

    hRapiResult = CeRapiInit();

    if (FAILED(hRapiResult))
    {
        return 1;
    }


    hFind = CeFindFirstFile( wszSrcFile, &wfd);

    if (INVALID_HANDLE_VALUE == hFind)
    {
		CeFindNextFile(hFind,&wfd);
         AfxMessageBox(L"Windows CE file does not exist11");
        return 1; //TEXT("Windows CE file does not exist\n");
    }    
    //CeFindClose( hFind);

    if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
    {
		AfxMessageBox(L"Windows CE file specifies a directory");
        return 1; // TEXT("Windows CE file specifies a directory\n");
    }

    dwAttr = GetFileAttributes( tszDestFile);
    
    if (0xFFFFFFFF  != dwAttr)
    {
        if (dwAttr & FILE_ATTRIBUTE_DIRECTORY)
        {
            //Just a directory was specified so we will have to get the filename from the src spec.
            hr = StringCchCat(tszDestFile, ARRAYSIZE(tszDestFile), TEXT("\\"));
            if(FAILED(hr))
            {
                return 1;
            }



            hr = StringCchCat(tszDestFile+wcslen(tszDestFile), ARRAYSIZE(tszDestFile), wfd.cFileName);
            if(FAILED(hr))
            {
                return 1;
            }

        }
       
    }

    hSrc = CeCreateFile(
                wszSrcFile,
                GENERIC_READ,
                FILE_SHARE_READ,
                NULL,
                OPEN_EXISTING,
                FILE_ATTRIBUTE_NORMAL,
                NULL);
    if (INVALID_HANDLE_VALUE == hSrc)
    {
       AfxMessageBox(L"Unable to open WinCE file");
        return 1; // TEXT("Unable to open Windows CE file");
    }

    hDest = CreateFile(
                tszDestFile,
                GENERIC_WRITE,
                FILE_SHARE_READ,
                NULL,
                CREATE_ALWAYS,
                FILE_ATTRIBUTE_NORMAL,
                NULL);

    if (INVALID_HANDLE_VALUE == hDest)
    {
       AfxMessageBox(L"Unable to open host/destination file.");
        return 1; //TEXT("Unable to open host/destination file");
    }

    //wprintf( TEXT("Copying WCE:%s to %s\n"), wszSrcFile, tszDestFile);
	logtmp.Format(L"%s to %s",source,dest);
	LOG("Copy File: " + logtmp);
    
    do
    {
        if (CeReadFile(
                hSrc,
                &Buffer,
                sizeof(Buffer),
                &dwNumRead,
                NULL))
        {
            if (!WriteFile(
                    hDest,
                    &Buffer,
                    dwNumRead,
                    &dwNumWritten,
                    NULL))
            {
                
                goto FatalError; //_tprintf( TEXT("Error !!! Writing hostfile"));
            }
        }
        else
        {
           
            goto FatalError; // _tprintf( TEXT("Error !!! Reading Windows CE file"));
        }
        //_tprintf( TEXT("."));                                        
    } while (dwNumRead);
   // _tprintf( TEXT("\n"));    
FatalError:
    CeCloseHandle( hSrc);
    CloseHandle (hDest);
  //  _tprintf( TEXT("Closing connection ..."));
    CeRapiUninit();
  //  _tprintf( TEXT("Done\n"));

    return 0;
}




/********************************************************************************************************
*	
*		Put File On Device
*
*********************************************************************************************************/	

int PutFileOnDevice(CString source,CString dest,int flags) {

TCHAR tszSrcFile[MAX_PATH];
WCHAR wszDestFile[MAX_PATH];
BYTE  Buffer[4096];

CStringA sourcefile(source);
CStringA destfile(dest);
CString logtmp;

    HANDLE hSrc, hDest, hFind;
    WIN32_FIND_DATA wfd;
    HRESULT hRapiResult;
    HRESULT hr;
    DWORD dwAttr, dwNumRead, dwNumWritten;
    int nResult;
    
   
#ifdef UNICODE
        nResult = MultiByteToWideChar(
                    CP_ACP,    
                    MB_PRECOMPOSED,
                    sourcefile,
                    sourcefile.GetLength()+1,
				    tszSrcFile,
                    fARRAYSIZE(tszSrcFile));
        if(0 == nResult)
        {
            return 1;
        }
#else
        hr = StringCchCopy(tszSrcFile, ARRAYSIZE(tszSrcFile), argv[1]);
        if(FAILED(hr))
        {
            return 1;
        }
#endif
    

    nResult = MultiByteToWideChar(
                CP_ACP,    
                MB_PRECOMPOSED,
                destfile,
                destfile.GetLength()+1,
				wszDestFile,
                fARRAYSIZE(wszDestFile));
    if(0 == nResult)
    {
        return 1;
    }

    hFind = FindFirstFile( tszSrcFile, &wfd);

    if (INVALID_HANDLE_VALUE == hFind)
    {
        //_tprintf( TEXT("Source/host file does not exist\n"));
        return 1;
    }    
    FindClose( hFind);

    if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
    {
        //_tprintf( TEXT("Source/host file specifies a directory\n"));
        return 1;
    }

   // _tprintf( TEXT("Connecting to Windows CE..."));
    
    hRapiResult = CeRapiInit();

    if (FAILED(hRapiResult))
    {
       // _tprintf( TEXT("Failed\n"));
        return 1;
    }

   // _tprintf( TEXT("Success\n"));

    dwAttr = CeGetFileAttributes( wszDestFile);
    
    if (0xFFFFFFFF  != dwAttr)
    {
        if (dwAttr & FILE_ATTRIBUTE_DIRECTORY)
        {
            //Just a directory was specified so we will have to get the filename from the src spec.
            hr = StringCchCatW(wszDestFile, ARRAYSIZE(wszDestFile), L"\\");
            if(FAILED(hr))
            {
                return 1;
            }
#ifdef UNICODE
            hr = StringCchCatW(wszDestFile, ARRAYSIZE(wszDestFile), wfd.cFileName);
            if(FAILED(hr))
            {
                return 1;
            }
#else
            nResult = MultiByteToWideChar(
                        CP_ACP,    
                        MB_PRECOMPOSED,
                        wfd.cFileName,
                        strlen(wfd.cFileName)+1,
                        wszDestFile+wcslen(wszDestFile),
                        ARRAYSIZE(wszDestFile)-wcslen(wszDestFile));
            if(0 == nResult)
            {
                return 1;
            }
#endif
        }
       // else
      //  {
          //  _tprintf( TEXT("File already exists.  Do you want to overwrite ? (Y/N)"));
           // if ( 'N' == toupper(getchar()))
          //  {
           //     _tprintf( TEXT("File not updateded\n"));
          //      goto CloseConnection;
          //  }
       // }
    }

    hSrc = CreateFile(
                tszSrcFile,
                GENERIC_READ,
                FILE_SHARE_READ,
                NULL,
                OPEN_EXISTING,
                FILE_ATTRIBUTE_NORMAL,
                NULL);
    if (INVALID_HANDLE_VALUE == hSrc)
    {
      //  _tprintf( TEXT("Unable to open source/host file"));
        return 1;
    }

    hDest = CeCreateFile(
                wszDestFile,
                GENERIC_WRITE,
                FILE_SHARE_READ,
                NULL,
                CREATE_ALWAYS,
                FILE_ATTRIBUTE_NORMAL,
                NULL);

    if (INVALID_HANDLE_VALUE == hDest )
    {
     //   _tprintf( TEXT("Unable to open WinCE file"));
        return 1;
    }
#ifdef UNICODE
   // wprintf( TEXT("Copying %s to WCE:%s\n"), tszSrcFile, wszDestFile);
	logtmp.Format(L"%s to %s",source,dest);
	LOG("Copy File " + logtmp);
#else
    printf( TEXT("Copying %s to WCE:%S\n"), tszSrcFile, wszDestFile);
#endif
    
    do
    {
        if (ReadFile(
                hSrc,
                &Buffer,
                sizeof(Buffer),
                &dwNumRead,
                NULL))
        {
            if (!CeWriteFile(
                    hDest,
                    &Buffer,
                    dwNumRead,
                    &dwNumWritten,
                    NULL))
            {
        //        _tprintf( TEXT("Error !!! Writing WinCE file"));
                return 1;
            }
        }
        else
        {
      //      _tprintf( TEXT("Error !!! Reading source file"));
           return 1;
        }
        _tprintf( TEXT("."));                                        
    } while (dwNumRead);
    _tprintf( TEXT("\n"));    
FatalError:
    CeCloseHandle( hDest);
    CloseHandle (hSrc);
CloseConnection:
 //   _tprintf( TEXT("Closing connection ..."));
   // CeRapiUninit();
 //   _tprintf( TEXT("Done\n"));

    return 0;
}


int ArchiveFile(CString whatfile,CString wherefile) {
//return values: 0 = No file to archive, 1 = Archive failed, 2 = Archive Success

	CFileException e;
	TCHAR strError[512] = _T("");
	SYSTEMTIME LocalSystemTime;
	GetLocalTime(&LocalSystemTime);		
	int revnum;
	CString fname,path,tmpfilename,date,n;
	
	if (GetFileAttributes(whatfile) != 0xFFFFFFFF) {
		CFile whatf(whatfile, CFile::modeRead);
		fname = whatf.GetFileName();
		whatf.Close();
	}
	else 
		return 0; //Return 0 because we couldn't find the file to archive..


	path = wherefile;
	date.Format(L"%02d-%02d-%02d", LocalSystemTime.wYear, LocalSystemTime.wMonth,LocalSystemTime.wDay);
	tmpfilename = path + date + fname;

	if (GetFileAttributes(tmpfilename)) {
		n="1";
		for (revnum=1;GetFileAttributes( path + date + fname + n ) != -1;revnum++) {
		n.Format(L"%i",revnum);
		}

		tmpfilename = path + date + fname + n;
	}
	
	if (GetFileAttributes(whatfile) !=  0xFFFFFFFF) {
		if (CopyFileEx(whatfile,tmpfilename,NULL,NULL,FALSE,COPY_FILE_FAIL_IF_EXISTS) ==0) {
			ErrorExit(L"CopyFileEx");
			return 1;
			}
		else return 2;
	
	}
	else return 1;

	LOG("ArchiveFile: " + whatfile + L" to "+ tmpfilename);
return 0;
}

CString CountLines(CString filename) {
	CStdioFile f;
	CStringW line,lc;
	line = "Batman.";
	int x;
	x = 0;
	lc = "";
	
	if (!f.Open(filename,CStdioFile::modeRead | CFile::typeText))
		return _T("ERROR");
	while (f.ReadString(line) != NULL) { 
		
	x++;
	}
	f.Close();
	lc.Format(L"%d",x);
	LOG("LineCount: " + lc);
return lc;
}


bool CopyFilePreApp(CString fromwhere,CString towhere,CString pre,CString app) {
	if (!pre) pre = "";
	if (!app) app = "";

	CString fname,fpath,fullnamepath;
	//if (GetFileAttributes(towhere) != -1) {
		CFile whatf(towhere, CFile::modeRead | CFile::modeCreate);

		fname = whatf.GetFileName();
		fpath = whatf.GetFilePath();
		whatf.Close();
		fullnamepath = GetFolderOnly(fpath)+ L'\\' + pre + fname + app;
		
		
	//}

	LOG("CopyFileEx(" + fromwhere + L", " + fullnamepath + L"...");
	
	if (CopyFileEx(fromwhere,fullnamepath,NULL,NULL,FALSE,0) == 0) {
		return false;
	}

return true;
}

CString GetFolderOnly(CString Path)
{

// Strip off the file name so we can direct the file scanning dialog to go
// back to the same directory as before.
CString temp = Path; // Force CString to make a copy
::PathRemoveFileSpec(temp.GetBuffer(0));
temp.ReleaseBuffer(-1);
return temp;
}