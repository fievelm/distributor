#pragma once
#include "Distributor.h"
#include "string.h"
#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
#include <shlwapi.h>


#define NOFILEONCE 1



int GetFileFromDevice(CString sourcefile,CString destfile,int flag);
int PutFileOnDevice(CString sourcefile,CString destfile,int flag);
int ArchiveFile(CString whatfile,CString wherefile);
bool CopyFilePreApp(CString fromwhere,CString towhere,CString pre,CString app);
CString CountLines(CString filename);
CString GetFolderOnly(CString Path);
HRESULT TryRapiConnect(DWORD dwTimeOut);