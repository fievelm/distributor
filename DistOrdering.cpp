// DistOrdering.cpp : implementation file
//

#include "stdafx.h"

#include "Distributor.h"
#include "DistOrdering.h"


// DistOrdering dialog

IMPLEMENT_DYNAMIC(DistOrdering, CDialog)

DistOrdering::DistOrdering(CWnd* pParent /*=NULL*/)
	: CDialog(DistOrdering::IDD, pParent)
{
	//RAPIINIT rapidev;
	//CeRapiInit();

SetDlgItemText(IDC_EDIT1,L"");




}

DistOrdering::~DistOrdering()
{
	
}

void DistOrdering::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(DistOrdering, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, &DistOrdering::OnBnClickedButton1)
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_BUTTON2, &DistOrdering::OnBnClickedButton2)
END_MESSAGE_MAP()


// DistOrdering message handlers
BOOL DistOrdering::OnEraseBkgnd(CDC* pDC) {
	BOOL bRet = CDialog::OnEraseBkgnd(pDC);
    // let the things happen after this call else it will erase everything

	CBitmap bitmap;
    // loading the bitmap
    bitmap.LoadBitmap(IDB_BITMAP3);
    // get the bitmap size
    BITMAP bmp;
    bitmap.GetBitmap(&bmp);
 
    // create memory DC
    CDC memDc;
    memDc.CreateCompatibleDC(pDC);
    memDc.SelectObject(&bitmap);
 
    // drawing into the dialog
    pDC->BitBlt(0,0,bmp.bmWidth,bmp.bmHeight,&memDc,0,0,SRCCOPY);
return bRet;
}




void DistOrdering::OnBnClickedButton1()  //Pull Orders
{
	CString lc;

	SetDlgItemText(IDC_EDIT1,L"Working...");
	ArchiveFile(SETTINGS(ORDERSSERVER),SETTINGS(ORDERSARCHIVE));
	GetFileFromDevice(SETTINGS(ORDERSDEVICE),SETTINGS(PULLTMP),0);
	lc = CountLines(SETTINGS(PULLTMP));
	CopyFilePreApp(SETTINGS(PULLTMP),SETTINGS(ORDERSSERVER),SETTINGS(ORDERSPREPEND),SETTINGS(ORDERSAPPEND));
	LOG("Get orders from " + SETTINGS(ORDERSDEVICE) + L" to " + SETTINGS(ORDERSPREPEND) + SETTINGS(ORDERSSERVER) + SETTINGS(ORDERSAPPEND) );
	SetDlgItemText(IDC_EDIT1,lc + L" Records Done!");
}

void DistOrdering::OnBnClickedButton2() //Push Orders
{
	
	CString lc;
	SetDlgItemText(IDC_EDIT1,L"Working...");
	GetFileFromDevice(SETTINGS(ORDERSDEVICE),SETTINGS(PULLTMP),0);
	lc= CountLines(SETTINGS(PULLTMP));
	ArchiveFile(SETTINGS(PULLTMP),SETTINGS(TAVARCHIVE));

	CopyFilePreApp(SETTINGS(ORDERSSERVER),SETTINGS(PUSHTMP),L"",L"");
	PutFileOnDevice(SETTINGS(PUSHTMP),SETTINGS(ORDERSDEVICE),0);
	LOG("Copied " + SETTINGS(ORDERSSERVER) + L" to " + SETTINGS(ORDERSDEVICE));
	SetDlgItemText(IDC_EDIT1,lc + L" Records Done!");
}
