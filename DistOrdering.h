#pragma once


// DistOrdering dialog

class DistOrdering : public CDialog
{
	DECLARE_DYNAMIC(DistOrdering)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
public:
	DistOrdering(CWnd* pParent = NULL);   // standard constructor
	virtual ~DistOrdering();
	
// Dialog Data
	enum { IDD = IDD_ORDERING };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg void OnPaint();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
};
