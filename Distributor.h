// Distributor.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include <strsafe.h>
#include "resource.h"		// main symbols
#include "Inc\Rapi.h"
#include "BtnST.h"
#include "DistOrdering.h"
#include "CEFileTransfers.h"
#include "logfile.h"
#include "SettingsFile.h"
#include "InventoryDlg.h"
#include "WipDlg.h"
#include "DualWIP.h"


#define DVERSION L"1.0.2.0"
#define DEBUG1 AfxMessageBox(L"DEBUG 1",NULL,NULL);
#define DEBUG2 AfxMessageBox(L"DEBUG 2",NULL,NULL);
#define DEBUG3 AfxMessageBox(L"DEBUG 3",NULL,NULL);
#define DEBUG4 AfxMessageBox(L"DEBUG 4",NULL,NULL);

// CDistributorApp:
// See Distributor.cpp for the implementation of this class
//
void ErrorExit(LPTSTR lpszFunction);
class CDistributorApp : public CWinApp
{
public:
	CDistributorApp();
	
	//bool LoadSettings();
	bool ModifySetting(CString what, CString value);
	//bool SaveSettings();
	void SetDefaultSettings();
	CString DSettings[SETTINGSMAX];

	
	WipDlg Wdlg;
	InventoryDlg Invdlg;
	DualWIP dwip;


// Overrides
	public:
	virtual BOOL InitInstance();


	protected:

	/*Server File Locations*/
	CString ServerOrdersCSV;
	CString ServerPartsListCSV;
	//inventory lists
	CString ServerCartsCSV;
	CString ServerJobsListCSV;
	CString ServerLOBCSV;

	/*Client File Locations*/


// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CDistributorApp theApp;


