// DistributorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Distributor.h"
#include "DistributorDlg.h"

#define D_BTNOVER 1929212

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();
	
// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	


}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CDistributorDlg dialog




CDistributorDlg::CDistributorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDistributorDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_DIST);



	



}


CDistributorDlg::~CDistributorDlg() {
 //CeRapiUninit();
	LOG("Closed Application");
	closelog();
}
void CDistributorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON1, btn_ordering);
	DDX_Control(pDX, IDC_BUTTON2, btn_inventory);
	DDX_Control(pDX, IDC_BUTTON3, btn_wip);
	DDX_Control(pDX, IDC_BUTTON4, btn_cart);
	DDX_Control(pDX, IDC_BUTTON7, btn_cheer);
	DDX_Control(pDX, IDC_BUTTON9, btn_settings);
}

BEGIN_MESSAGE_MAP(CDistributorDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON1, &CDistributorDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON5, &CDistributorDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &CDistributorDlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &CDistributorDlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON2, &CDistributorDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON9, &CDistributorDlg::OnBnClickedButton9)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_BUTTON3, &CDistributorDlg::OnBnClickedButton3)
END_MESSAGE_MAP()


// CDistributorDlg message handlers


void CDistributorDlg::setisconnected() {

	if (SETTINGS(DEBUGMODE) == L"YES") { this->isconnected = true; return; }

	if (this->isconnected == true) {
		GetDlgItem(IDC_BUTTON1)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON2)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON3)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON4)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON7)->EnableWindow(TRUE);
		}

	if (this->isconnected == false) {
		GetDlgItem(IDC_BUTTON1)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON2)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON3)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON4)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON7)->EnableWindow(FALSE);
	}


}


BOOL CDistributorDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	LoadSettings(L"settings.txt");
	openlog();
	LOG("Started Application");
	SetDlgItemText(IDC_STATIC, DVERSION);
	//CeRapiInit();
	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	

	isconnected = false;
	setisconnected();

	/*
	DDX_Control(pDX, IDC_BUTTON1, btn_ordering);
	DDX_Control(pDX, IDC_BUTTON2, btn_inventory);
	DDX_Control(pDX, IDC_BUTTON3, btn_wip);
	DDX_Control(pDX, IDC_BUTTON4, btn_cart);
	DDX_Control(pDX, IDC_BUTTON7, btn_cheer);
	DDX_Control(pDX, IDC_BUTTON9, btn_settings);
	*/
	btn_inventory.SetColor(0,D_BTNOVER,TRUE);
	btn_inventory.SetIcon(IDI_INVENTORY,IDI_INVENTORY);
	btn_ordering.SetColor(0,D_BTNOVER,TRUE);
	btn_ordering.SetIcon(IDI_CLIP,IDI_CLIP);
	btn_wip.SetColor(0,D_BTNOVER,TRUE);
	btn_wip.SetIcon(IDI_ICON3,IDI_ICON3);
	btn_cart.SetColor(0,D_BTNOVER,TRUE);
	btn_cheer.SetColor(0,D_BTNOVER,TRUE);
	btn_settings.SetColor(0,D_BTNOVER,TRUE);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDistributorDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CDistributorDlg::OnPaint()
{
	

	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting
		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);
		
		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		
		CDialog::OnPaint();
 
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDistributorDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CDistributorDlg::OnBnClickedButton1()
{

	DistOrdering d_orderscreen;
	d_orderscreen.DoModal();

//CeCreateFile(L"\\RAPI.txt",GENERIC_READ | GENERIC_WRITE,NULL,NULL,CREATE_NEW,FILE_ATTRIBUTE_NORMAL,NULL);

}



BOOL CDistributorDlg::OnEraseBkgnd(CDC* pDC) {
	BOOL bRet = CDialog::OnEraseBkgnd(pDC);
    // let the things happen after this call else it will erase everything

	CBitmap bitmap;
    // loading the bitmap
    bitmap.LoadBitmap(IDB_BITMAP1);
    // get the bitmap size
    BITMAP bmp;
    bitmap.GetBitmap(&bmp);
 
    // create memory DC
    CDC memDc;
    memDc.CreateCompatibleDC(pDC);
    memDc.SelectObject(&bitmap);
 
    // drawing into the dialog
    pDC->BitBlt(0,0,bmp.bmWidth,bmp.bmHeight,&memDc,0,0,SRCCOPY);
return bRet;
}



HRESULT CDistributorDlg::TryRapiConnect(DWORD dwTimeOut)
{
    HRESULT      hr = E_FAIL;
    RAPIINIT     riCopy;
    bool         fInitialized = false;

    ZeroMemory(&riCopy, sizeof(riCopy));
    riCopy.cbSize = sizeof(riCopy);

    hr = CeRapiInitEx(&riCopy);
    if (SUCCEEDED(hr))
    {
        DWORD dwRapiInit = 0;
        fInitialized = true;

        dwRapiInit = WaitForSingleObject(
                    riCopy.heRapiInit,
                    dwTimeOut);
        if (WAIT_OBJECT_0 == dwRapiInit)
        {
            //  heRapiInit signaled:
            // set return error code to return value of RAPI Init function
            hr = riCopy.hrRapiInit;  
        }
        else if (WAIT_TIMEOUT == dwRapiInit)
        {
            // timed out: device is probably not connected
            // or not responding
            hr = HRESULT_FROM_WIN32(ERROR_TIMEOUT);
        }
        else
        {
            // WaitForSingleObject failed
            hr = HRESULT_FROM_WIN32(GetLastError());
        }
    }

   if (fInitialized && FAILED(hr))
   {
       CeRapiUninit();
   }
    return hr;
}

void CDistributorDlg::OnBnClickedButton5()
{
	HRESULT rapicon = this->TryRapiConnect(500);
	CString r;

	if (rapicon == 0) {
		r = L"Connected!";
		isconnected = true;
	}
	else {
		r = L"Connection Error.";
		isconnected = false;
	}
		setisconnected();

		SetDlgItemText(IDC_EDIT1,r);
}

void CDistributorDlg::OnBnClickedButton6()
{
	//CeRapiUninit();
	SetDlgItemText(IDC_EDIT1,L"Disconnected.");
	isconnected = false;
	setisconnected();
}

void CDistributorDlg::OnBnClickedButton7()
{
	AfxMessageBox(L"Don\'t Click me!");
}

void CDistributorDlg::OnBnClickedButton3()
{
	
	theApp.Wdlg.DoModal();
}

void CDistributorDlg::OnBnClickedButton2()
{
	
	theApp.Invdlg.DoModal();
}

void CDistributorDlg::OnBnClickedButton9()
{
	// TODO: Add your control notification handler code here
}
