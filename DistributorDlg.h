// DistributorDlg.h : header file
//

#pragma once
#ifndef DistributorDlgDef

// CDistributorDlg dialog
class CDistributorDlg : public CDialog
{
// Construction
public:
	CDistributorDlg(CWnd* pParent = NULL);	// standard constructor
	CDistributorDlg::~CDistributorDlg();
	HRESULT TryRapiConnect(DWORD dwTimeOut);
	//HRESULT TryRapiConnect(DWORD dwTimeOut);
	
	


// Dialog Data
	enum { IDD = IDD_DISTRIBUTOR_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton7();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton9();
	bool isconnected;
	void setisconnected();

	CButtonST btn_ordering,btn_inventory,btn_wip,btn_cart,btn_cheer,btn_settings;
	afx_msg void OnBnClickedButton3();
};



#endif