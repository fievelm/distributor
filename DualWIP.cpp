// DualWIP.cpp : implementation file
//

#include "stdafx.h"
#include "Distributor.h"
#include "DistributorDlg.h"
#include "DualWIP.h"
#include "CEFileTransfers.h"
#include "WipDlg.h"


#define D_BTNOVER 1929212

// DualWIP dialog

IMPLEMENT_DYNAMIC(DualWIP, CDialog)

DualWIP::DualWIP(CWnd* pParent /*=NULL*/)
	: CDialog(DualWIP::IDD, pParent)
{

}

DualWIP::~DualWIP()
{
}

void DualWIP::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PRIMREC, static_primary_records);
	DDX_Control(pDX, IDC_SECREC, static_secondary_records);
	DDX_Control(pDX, IDC_DUPES, static_duplicates);
	DDX_Control(pDX, IDC_STARTDUAL, btn_startdual);
	DDX_Control(pDX, IDC_CNCTPRIM, btn_cnctprim);
	DDX_Control(pDX, IDC_UPPRIM, btn_upprim);
	DDX_Control(pDX, IDC_CNCTSEC, btn_cnctsec);
	DDX_Control(pDX, IDC_UPSEC, btn_upsec);
	DDX_Control(pDX, IDC_MERGE, btn_merge);
	DDX_Control(pDX, IDC_UPLOAD, btn_upload);

}


BEGIN_MESSAGE_MAP(DualWIP, CDialog)
	ON_BN_CLICKED(IDC_STARTDUAL, &DualWIP::OnBnClickedStartdual)
	ON_BN_CLICKED(IDC_CNCTPRIM, &DualWIP::OnBnClickedCnctprim)
	ON_BN_CLICKED(IDC_UPPRIM, &DualWIP::OnBnClickedUpprim)
	ON_BN_CLICKED(IDC_CNCTSEC, &DualWIP::OnBnClickedCnctsec)
	ON_BN_CLICKED(IDC_UPSEC, &DualWIP::OnBnClickedUpsec)
	ON_BN_CLICKED(IDC_MERGE, &DualWIP::OnBnClickedMerge)
	ON_BN_CLICKED(IDC_UPLOAD, &DualWIP::OnBnClickedUpload)
END_MESSAGE_MAP()


BOOL DualWIP::OnInitDialog()
{
	CDialog::OnInitDialog();
	btn_startdual.SetColor(0,D_BTNOVER,TRUE);
	btn_cnctprim.SetColor(0,D_BTNOVER,TRUE); btn_cnctprim.EnableWindow(FALSE);
	btn_upprim.SetColor(0,D_BTNOVER,TRUE); btn_upprim.EnableWindow(FALSE);
	btn_cnctsec.SetColor(0,D_BTNOVER,TRUE); btn_cnctsec.EnableWindow(FALSE);
	btn_upsec.SetColor(0,D_BTNOVER,TRUE); btn_upsec.EnableWindow(FALSE);
	btn_merge.SetColor(0,D_BTNOVER,TRUE); btn_merge.EnableWindow(FALSE);
	btn_upload.SetColor(0,D_BTNOVER,TRUE); btn_upload.EnableWindow(FALSE);
return true;

}



// DualWIP message handlers

void DualWIP::OnBnClickedStartdual()
{
	btn_startdual.EnableWindow(FALSE);
	btn_cnctprim.EnableWindow(TRUE);
}

void DualWIP::OnBnClickedCnctprim()
{
	if (TryRapiConnect(500) == 0) { btn_cnctprim.EnableWindow(FALSE); btn_upprim.EnableWindow(TRUE); }
}

void DualWIP::OnBnClickedUpprim()
{
	CString lc;
	int lctoi;
	
	if (GetFileFromDevice(SETTINGS(WIPDEVICE),SETTINGS(PULLTMP),0) == 1)
	{
		AfxMessageBox(L"Error Reading Device.",NULL,NULL);
		return;
	}

	ShellExecute(NULL,NULL,SETTINGS(STRINGCONVERTER), SETTINGS(WIPTEMP) + L' ' + L"db1.xml" + L' ' + SETTINGS(STRINGCARGSSETTING) ,NULL ,SW_SHOW);

	lctoi = theApp.Wdlg.openwipdb(L"db1.xml", &theApp.Wdlg.PrimaryWIPdb);
	lc.Format(L"%d",lctoi);

	static_primary_records.SetWindowTextW(L"Primary Device Records: " + lc);

	if (lctoi > 0) {
		btn_upprim.EnableWindow(FALSE);
		btn_cnctsec.EnableWindow(TRUE);
	}

}

void DualWIP::OnBnClickedCnctsec()
{
	CeRapiUninit();

	if (TryRapiConnect(500) == 0) {btn_cnctsec.EnableWindow(FALSE); btn_upsec.EnableWindow(TRUE); }
}

void DualWIP::OnBnClickedUpsec()
{
	CString lc;
	int lctoi;
	
	if (GetFileFromDevice(SETTINGS(WIPDEVICE),SETTINGS(PULLTMP),0) == 1)
	{
		AfxMessageBox(L"Error Reading Device.",NULL,NULL);
		return;
	}

	ShellExecute(NULL,NULL,SETTINGS(STRINGCONVERTER), SETTINGS(WIPTEMP) + L' ' + L"db2.xml" + L' ' + SETTINGS(STRINGCARGSSETTING) ,NULL ,SW_SHOW);
	
	lctoi = theApp.Wdlg.openwipdb(L"db2.xml",&theApp.Wdlg.SecondaryWIPdb);
	lc.Format(L"%d",lctoi);

	static_secondary_records.SetWindowTextW(L"Secondary Device Records: " + lc);

	if (lctoi > 0) {
		btn_upsec.EnableWindow(FALSE);
		btn_merge.EnableWindow(TRUE);
	}

}

void DualWIP::OnBnClickedMerge()
{
	
	/************************************************************/

	
	CString pday,pmonth,pyear,sday,smonth,syear;
	CString phour,pminute,psecond,shour,sminute,ssecond;
	CString s;
	COleDateTime pdate,sdate;
	int matches;

	
	matches = 0;

	for (size_t j=0; j<theApp.Wdlg.SecondaryWIPdb.size(); ++j)
		{
			int match = 0;
			for (size_t i=0; i<theApp.Wdlg.PrimaryWIPdb.size(); ++i)
			{
				if (theApp.Wdlg.PrimaryWIPdb[i][2] == theApp.Wdlg.SecondaryWIPdb[j][2])
				{
					/*   WE HAVE A MATCH!   */ 
					match++;
					/*
					s.Format(L"%02d,%02d,%02d", LocalSystemTime.wDay,LocalSystemTime.wMonth,LocalSystemTime.wYear);
					s.Format(L"%02d:%02d.%02d",LocalSystemTime.wHour,LocalSystemTime.wMinute,LocalSystemTime.wSecond);
					*/
					AfxExtractSubString(pday,theApp.Wdlg.PrimaryWIPdb[i][3],0,L',');
					AfxExtractSubString(pmonth,theApp.Wdlg.PrimaryWIPdb[i][3],1,L',');
					AfxExtractSubString(pyear,theApp.Wdlg.PrimaryWIPdb[i][3],2,L',');

					AfxExtractSubString(phour,theApp.Wdlg.PrimaryWIPdb[i][4],0,L':');
					AfxExtractSubString(pminute,theApp.Wdlg.PrimaryWIPdb[i][4],1,L':');

					AfxExtractSubString(sday,theApp.Wdlg.SecondaryWIPdb[j][3],0,L',');
					AfxExtractSubString(smonth,theApp.Wdlg.SecondaryWIPdb[j][3],1,L',');
					AfxExtractSubString(syear,theApp.Wdlg.SecondaryWIPdb[j][3],2,L',');
					
					AfxExtractSubString(shour,theApp.Wdlg.SecondaryWIPdb[j][4],0,L':');
					AfxExtractSubString(sminute,theApp.Wdlg.SecondaryWIPdb[j][4],1,L':');
					
					pdate.ParseDateTime(pmonth + L'/' + pday + L'/' + pyear + L' ' + phour + L':'+ pminute + L":00");
					sdate.ParseDateTime(smonth + L'/' + sday + L'/' + syear + L' ' + shour + L':'+ sminute + L":00");
					matches++;

					if (pdate > sdate) {
						break; /*Primary DB is master db, so do nothing.*/
						}
					else {
						/*Move secondary data to primary*/
						theApp.Wdlg.PrimaryWIPdb[i] = theApp.Wdlg.SecondaryWIPdb[j];
						break;
						}
	
					}//no match if we reach this point.
			}//for primary
			if (match == 0) {
				theApp.Wdlg.PrimaryWIPdb.push_back(theApp.Wdlg.SecondaryWIPdb[j]);
			}
	}//for secondary

	s.Format(L"%d",matches);
	AfxMessageBox(L"Matches: " +s,NULL,NULL);

	btn_merge.EnableWindow(FALSE);
	btn_upload.EnableWindow(TRUE);
}

void DualWIP::OnBnClickedUpload()
{
	btn_upload.EnableWindow(FALSE);
	theApp.Wdlg.PrimaryWIPdb.clear();
	theApp.Wdlg.SecondaryWIPdb.clear();
	// TODO: Add your control notification handler code here
}
