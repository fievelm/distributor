#pragma once
#include "afxwin.h"


// DualWIP dialog

class DualWIP : public CDialog
{
	DECLARE_DYNAMIC(DualWIP)

public:
	DualWIP(CWnd* pParent = NULL);   // standard constructor
	virtual ~DualWIP();

// Dialog Data
	enum { IDD = IDD_DIALOG4 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedStartdual();
	afx_msg void OnBnClickedCnctprim();
	afx_msg void OnBnClickedUpprim();
	afx_msg void OnBnClickedCnctsec();
	afx_msg void OnBnClickedUpsec();
	afx_msg void OnBnClickedMerge();
	afx_msg void OnBnClickedUpload();

	CStatic static_primary_records;
	CStatic static_secondary_records;
	CStatic static_duplicates;


	CButtonST btn_startdual,btn_cnctprim,btn_upprim,btn_cnctsec,btn_upsec,btn_merge,btn_upload;
};
