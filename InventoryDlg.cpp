// InventoryDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Distributor.h"
#include "InventoryDlg.h"


#define D_BTNOVERI 1929212
#define D_BTNOVERIUPLOAD 11229212

// InventoryDlg dialog

IMPLEMENT_DYNAMIC(InventoryDlg, CDialog)

InventoryDlg::InventoryDlg(CWnd* pParent /*=NULL*/)
	: CDialog(InventoryDlg::IDD, pParent)
{



}

void InventoryDlg::UpdatePartsTime() {

CFileStatus cfs;
CString datetime;
//CString debugtime;

SetDlgItemText(IDC_MYPARTS,L"None");
SetDlgItemText(IDC_SRVPARTS,L"None");

if (CFile::GetStatus(SETTINGS(PARTSFILESERVER),cfs) ) {
	datetime.Format(L"%02d-%02d-%02d / %02d:%02d",cfs.m_mtime.GetYear(),cfs.m_mtime.GetMonth(),cfs.m_mtime.GetDay(),cfs.m_mtime.GetHour(),cfs.m_mtime.GetMinute());
	SetDlgItemText(IDC_SRVPARTS,datetime);
}

//if (TryRapiConnect(900) == 0) {

CE_FIND_DATA wfd;
SYSTEMTIME wst = {0};


if (CeFindFirstFile( SETTINGS(PARTSFILEDEVICE), &wfd)) {
	//FileTimeToLocalFileTime(&wfd.ftCreationTime,&wfdlocal);
	FileTimeToSystemTime(&wfd.ftCreationTime,&wst);
	wst.wHour = wst.wHour - 8;
	
	datetime.Format(L"%02d-%02d-%02d / %02d:%02d",wst.wYear,wst.wMonth,wst.wDay,wst.wHour,wst.wMinute);
	SetDlgItemText(IDC_MYPARTS,datetime);
	}

}


void InventoryDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON1, btn_ahpaint);
	DDX_Control(pDX, IDC_BUTTON2, btn_cabinet);
	DDX_Control(pDX, IDC_BUTTON3, btn_consumables);
	DDX_Control(pDX, IDC_BUTTON4, btn_core);
	DDX_Control(pDX, IDC_BUTTON5, btn_corep8a);
	DDX_Control(pDX, IDC_BUTTON6, btn_freezers);
	DDX_Control(pDX, IDC_BUTTON7, btn_general);
	DDX_Control(pDX, IDC_BUTTON8, btn_cribmaster);
	DDX_Control(pDX, IDC_BUTTON9, btn_hardware);
	DDX_Control(pDX, IDC_BUTTON10, btn_metaldetails);
	DDX_Control(pDX, IDC_BUTTON11, btn_rawparts);
	DDX_Control(pDX, IDC_BUTTON12, btn_shipping);
	DDX_Control(pDX, IDC_BUTTON13, btn_shims);
	DDX_Control(pDX, IDC_EDIT2, statusedit);
	DDX_Control(pDX, IDC_BUTTON15, btn_upload);
}



BOOL InventoryDlg::OnInitDialog() {
	CDialog::OnInitDialog();

selectedinv = 99;

UpdatePartsTime();
	
/*
	DDX_Control(pDX, IDC_BUTTON1, btn_ahpaint);
	DDX_Control(pDX, IDC_BUTTON2, btn_cabinet);
	DDX_Control(pDX, IDC_BUTTON3, btn_consumables);
	DDX_Control(pDX, IDC_BUTTON4, btn_core);
	DDX_Control(pDX, IDC_BUTTON5, btn_corep8a);
	DDX_Control(pDX, IDC_BUTTON6, btn_freezers);
	DDX_Control(pDX, IDC_BUTTON7, btn_general);
	DDX_Control(pDX, IDC_BUTTON8, btn_cribmaster);
	DDX_Control(pDX, IDC_BUTTON9, btn_hardware);
	DDX_Control(pDX, IDC_BUTTON10, btn_metaldetails);
	DDX_Control(pDX, IDC_BUTTON11, btn_rawparts);
	DDX_Control(pDX, IDC_BUTTON12, btn_shipping);
	DDX_Control(pDX, IDC_BUTTON13, btn_shims);
*/
btn_ahpaint.SetColor(0,D_BTNOVERI,FALSE); btn_ahpaint.SetColor(4,D_BTNOVERI,TRUE);
btn_cabinet.SetColor(0,D_BTNOVERI,FALSE); btn_cabinet.SetColor(4,D_BTNOVERI,TRUE);
btn_consumables.SetColor(0,D_BTNOVERI,FALSE); btn_consumables.SetColor(4,D_BTNOVERI,TRUE);
btn_core.SetColor(0,D_BTNOVERI,FALSE); btn_core.SetColor(4,D_BTNOVERI,TRUE);
btn_corep8a.SetColor(0,D_BTNOVERI,FALSE); btn_corep8a.SetColor(4,D_BTNOVERI,TRUE); 
btn_freezers.SetColor(0,D_BTNOVERI,FALSE); btn_freezers.SetColor(4,D_BTNOVERI,TRUE);
btn_general.SetColor(0,D_BTNOVERI,FALSE); btn_general.SetColor(4,D_BTNOVERI,TRUE);
btn_cribmaster.SetColor(0,D_BTNOVERI,FALSE); btn_cribmaster.SetColor(4,D_BTNOVERI,TRUE);
btn_hardware.SetColor(0,D_BTNOVERI,FALSE); btn_hardware.SetColor(4,D_BTNOVERI,TRUE);
btn_metaldetails.SetColor(0,D_BTNOVERI,FALSE); btn_metaldetails.SetColor(4,D_BTNOVERI,TRUE);
btn_rawparts.SetColor(0,D_BTNOVERI,FALSE); btn_rawparts.SetColor(4,D_BTNOVERI,TRUE);
btn_shipping.SetColor(0,D_BTNOVERI,TRUE); btn_shims.SetColor(4,D_BTNOVERI,TRUE);
btn_shims.SetColor(0,D_BTNOVERI,TRUE); btn_shims.SetColor(4,D_BTNOVERI,TRUE);
btn_upload.SetColor(0,D_BTNOVERIUPLOAD,TRUE);
btn_upload.SetIcon(IDI_ICON4,IDI_ICON4);

return TRUE;
}


InventoryDlg::~InventoryDlg()
{
}

void InventoryDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		//dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

BOOL InventoryDlg::OnEraseBkgnd(CDC* pDC) {
	BOOL bRet = CDialog::OnEraseBkgnd(pDC);
    // let the things happen after this call else it will erase everything

	CBitmap bitmap;
    // loading the bitmap
    bitmap.LoadBitmap(IDB_BITMAP2);
    // get the bitmap size
    BITMAP bmp;
    bitmap.GetBitmap(&bmp);
 
    // create memory DC
    CDC memDc;
    memDc.CreateCompatibleDC(pDC);
    memDc.SelectObject(&bitmap);
 
    // drawing into the dialog
    pDC->BitBlt(0,0,bmp.bmWidth,bmp.bmHeight,&memDc,0,0,SRCCOPY);
return bRet;
}






BEGIN_MESSAGE_MAP(InventoryDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, &InventoryDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &InventoryDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &InventoryDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &InventoryDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &InventoryDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &InventoryDlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &InventoryDlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON8, &InventoryDlg::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON9, &InventoryDlg::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON10, &InventoryDlg::OnBnClickedButton10)
	ON_BN_CLICKED(IDC_BUTTON11, &InventoryDlg::OnBnClickedButton11)
	ON_BN_CLICKED(IDC_BUTTON12, &InventoryDlg::OnBnClickedButton12)
	ON_BN_CLICKED(IDC_BUTTON13, &InventoryDlg::OnBnClickedButton13)
	ON_BN_CLICKED(IDC_BUTTON14, &InventoryDlg::OnBnClickedButton14)
	ON_BN_CLICKED(IDC_BUTTON15, &InventoryDlg::OnBnClickedButton15)
	ON_BN_CLICKED(IDC_BUTTON16, &InventoryDlg::OnBnClickedButton16)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()


// InventoryDlg message handlers

void InventoryDlg::OnBnClickedButton1() { selectedinv =  INVAHPAINT;		ResetInvButtons(); /*btn_ahpaint.SetState(1);*/ }
void InventoryDlg::OnBnClickedButton2() { selectedinv =  INVCABINET;		ResetInvButtons();/* btn_cabinet.SetState(1);*/ }
void InventoryDlg::OnBnClickedButton3() { selectedinv =  INVCONSUMABLES;	ResetInvButtons();/* btn_consumables.SetState(1);*/ }
void InventoryDlg::OnBnClickedButton4() { selectedinv =  INVCORE;			ResetInvButtons(); /*btn_core.SetState(1);*/ }
void InventoryDlg::OnBnClickedButton5() { selectedinv =  INVCOREP8A;		ResetInvButtons();/* btn_corep8a.SetState(1);*/ }
void InventoryDlg::OnBnClickedButton6() { selectedinv =  INVFREEZERS;		ResetInvButtons(); /*btn_freezers.SetState(1);*/ }
void InventoryDlg::OnBnClickedButton7() { selectedinv =  INVGENERAL;		ResetInvButtons(); /*btn_general.SetState(1);*/ }
void InventoryDlg::OnBnClickedButton8() { selectedinv =  INVCRIBMASTER;		ResetInvButtons();/* btn_cribmaster.SetState(1);*/ }
void InventoryDlg::OnBnClickedButton9() { selectedinv =	 INVHARDWARE;		ResetInvButtons();/* btn_hardware.SetState(1);*/ }
void InventoryDlg::OnBnClickedButton10() { selectedinv = INVMETALDETAILS;	ResetInvButtons(); /*btn_metaldetails.SetState(1);*/ }
void InventoryDlg::OnBnClickedButton11() { selectedinv = INVRAWPARTS;		ResetInvButtons(); /*btn_rawparts.SetState(1);*/ }
void InventoryDlg::OnBnClickedButton12() { selectedinv = INVSHIPPING;		ResetInvButtons(); /*btn_shipping.SetState(1);*/ }
void InventoryDlg::OnBnClickedButton13() { selectedinv = INVSHIMS;			ResetInvButtons(); /*btn_shims.SetState(1); */}




void InventoryDlg::OnBnClickedButton14() {	

	CopyFilePreApp(SETTINGS(PARTSFILESERVER),SETTINGS(PUSHTMP),L"",L"");
	CeDeleteFile(SETTINGS(PARTSFILEDEVICE));

	PutFileOnDevice(SETTINGS(PUSHTMP),SETTINGS(PARTSFILEDEVICE),0);
	SetDlgItemText(IDC_EDIT2,L"Parts File Copied to Device.");

	UpdatePartsTime();
}



void InventoryDlg::OnBnClickedButton15() {  // UPLOAD INVENTORY FILE

	if (selectedinv == 99) return;
	LOG("Upload Inventory File");
	CString lc,tmp;
	tmp = SETTINGS(selectedinv);
	tmp.TrimLeft(L"\\");
	statusedit.SetWindowTextW(L"Uploading...");


	
/*
1) Check to make sure we have a spot to archive to.
*/		CString tempfloc;
	tempfloc = SETTINGS(INVARCHIVE);
	tempfloc.TrimRight(L"\\");
	CFileStatus status;
	if (!CFile::GetStatus(tempfloc,status)) {
		statusedit.SetWindowTextW(L"Invalid Archive Location. No Data uploaded. " +tempfloc);
		return;
	}
	LOG("Found Archive Loc");
	
/*
2) If we do, archive the old file.
*/
	tempfloc = SETTINGS(INVSERVER) +tmp;
	
	if (CFile::GetStatus(tempfloc,status)) {
		ArchiveFile(SETTINGS(INVSERVER) + tmp , SETTINGS(INVARCHIVE));
	}
	
	
/*
3) Get file from device to local machine
    If we fail, break.
*/
/*
	if (!CFile::GetStatus(SETTINGS(selectedinv),status) ) {
	statusedit.SetWindowTextW(L"Cannot access device file.");
	return;
	}*/

	if (GetFileFromDevice(SETTINGS(selectedinv),SETTINGS(PULLTMP),0) == 1)
	{
	AfxMessageBox(L"Error Reading Device.",NULL,NULL);
	return;
	}
	
	LOG("GetFileFromDevice(" + SETTINGS(selectedinv) + SETTINGS(PULLTMP));
	lc = CountLines(SETTINGS(PULLTMP));

/*
4) Copy file to server
   If we fail, break.
*/

	
	if (CopyFilePreApp(SETTINGS(PULLTMP),SETTINGS(INVSERVER) + tmp,SETTINGS(INVPREPEND),SETTINGS(INVAPPEND)) == false) {
		statusedit.SetWindowTextW(L"Could not copy file to server.");
		return;
	}
	/* Finally, if everything worked OK, delete the file from the Device.*/
	CeDeleteFile(SETTINGS(selectedinv));

	LOG("Get inv from " + SETTINGS(selectedinv) + L" to " + SETTINGS(INVPREPEND) + SETTINGS(INVSERVER) + tmp + SETTINGS(INVAPPEND) );
	statusedit.SetWindowTextW(lc + L" Records uploaded.");


	

}

void InventoryDlg::OnBnClickedButton16() {	
	CString lc;
	lc="File Does Not Exist.";

/*


Error Handling Goes Here.


*/

	if (GetFileFromDevice(SETTINGS(selectedinv),SETTINGS(PULLTMP),0) == 0)
		lc = CountLines(SETTINGS(PULLTMP));
	statusedit.SetWindowTextW(L"Records: " + lc);
}

void InventoryDlg::ResetInvButtons() {
/*	btn_ahpaint.SetState(0);
	btn_cabinet.SetState(0);
	btn_consumables.SetState(0);
	btn_core.SetState(0);
	btn_corep8a.SetState(0);
	btn_freezers.SetState(0);
	btn_general.SetState(0);
	btn_cribmaster.SetState(0);
	btn_hardware.SetState(0);
	btn_metaldetails.SetState(0);
	btn_rawparts.SetState(0);
	btn_shipping.SetState(0);
	btn_shims.SetState(0);*/
}