#pragma once
#include "afxwin.h"


// InventoryDlg dialog

class InventoryDlg : public CDialog
{
	DECLARE_DYNAMIC(InventoryDlg)

public:
	InventoryDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~InventoryDlg();
	int selectedinv;
	void ResetInvButtons();
	void UpdatePartsTime();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
// Dialog Data
	enum { IDD = IDD_DIALOG2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg void OnPaint();
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton7();
	afx_msg void OnBnClickedButton8();
	afx_msg void OnBnClickedButton9();
	afx_msg void OnBnClickedButton10();
	afx_msg void OnBnClickedButton11();
	afx_msg void OnBnClickedButton12();
	afx_msg void OnBnClickedButton13();
	afx_msg void OnBnClickedButton14();
	afx_msg void OnBnClickedButton15();
	afx_msg void OnBnClickedButton16();


	CEdit statusedit;
	CButtonST btn_ahpaint,btn_cabinet,btn_consumables,btn_core,btn_corep8a,btn_cribmaster,btn_freezers,btn_general,btn_hardware,btn_metaldetails,btn_rawparts,btn_shims,btn_shipping;
	CButtonST btn_upload;
};

