/*Log file app*/
#pragma once
#define logging
#include "stdafx.h"
#include <iostream>

#define LOG(x) logstring(_T(x))


bool openlog();
bool logstring(CString line);
bool closelog();
