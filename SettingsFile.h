#pragma once
#include "stdafx.h"
#include <iostream>
#include "Distributor.h"




#define LOGGING			0

#define ORDERSPREPEND	1
#define ORDERSAPPEND	2
#define ORDERSDEVICE	3
#define ORDERSSERVER	4
#define ORDERSARCHIVE	5

#define WIPAPPEND		6
#define WIPPREPEND		7
#define WIPDEVICE		8
#define WIPSERVER		9
#define WIPARCHIVE		10

#define INVAPPEND		11
#define INVPREPEND		12
#define INVAHPAINT		13
#define INVCABINET		14
#define INVCONSUMABLES	15
#define INVCORE			16
#define INVCOREP8A		17
#define INVCRIBMASTER	18
#define INVFREEZERS		19
#define INVGENERAL		20
#define INVHARDWARE		21
#define INVMETALDETAILS	22
#define INVRAWPARTS		23
#define INVSHIMS		24
#define INVSHIPPING		25
#define INVSERVER		26
#define INVARCHIVE		27

#define PARTSFILEDEVICE 28
#define PARTSFILESERVER 29

#define TAVARCHIVE 30

#define MASTERSETTINGS 31
#define PUSHTMP 32
#define PULLTMP 33
#define DEBUGMODE 34

#define STRINGCONVERTER 35
#define WIPTEMP 36
#define STRINGCARGSSETTING 37

#define SETTINGSMAX 40

#define CHECKSET(x,y)	if (name.CompareNoCase(x) == 0) {theApp.DSettings[(y)] = val;}
#define SETTINGS(x) theApp.DSettings[(x)]
#define SETTINGSFILE L"settings.txt"
#define STRINGCARGS SETTINGS(PULLTMP) + L" " + SETTINGS(WIPTEMP) + L" " + SETTINGS(STRINGCARGSSETTING)



bool LoadSettings(CString filename);
bool SaveSettings(CString filename);
