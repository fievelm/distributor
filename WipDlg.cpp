// WipDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Distributor.h"
#include "DistributorDlg.h"
#include "WipDlg.h"
#include <vector>
#include <iostream>
#include <sstream>
#include "DualWIP.h"

#define _UNICODE

#define WIPDBFILENAME L"wipdb.csv"


// WipDlg dialog

IMPLEMENT_DYNAMIC(WipDlg, CDialog)

WipDlg::WipDlg(CWnd* pParent /*=NULL*/)
	: CDialog(WipDlg::IDD, pParent)
{

}

WipDlg::~WipDlg()
{
}

void WipDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(WipDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, &WipDlg::OnBnClickedButton1)
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_BUTTON2, &WipDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &WipDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON5, &WipDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &WipDlg::OnBnClickedButton6)
END_MESSAGE_MAP()


BOOL WipDlg::OnEraseBkgnd(CDC* pDC) {
	BOOL bRet = CDialog::OnEraseBkgnd(pDC);
    // let the things happen after this call else it will erase everything

	CBitmap bitmap;
    // loading the bitmap
    bitmap.LoadBitmap(IDB_BITMAP2);
    // get the bitmap size
    BITMAP bmp;
    bitmap.GetBitmap(&bmp);
 
    // create memory DC
    CDC memDc;
    memDc.CreateCompatibleDC(pDC);
    memDc.SelectObject(&bitmap);
 
    // drawing into the dialog
    pDC->BitBlt(0,0,bmp.bmWidth,bmp.bmHeight,&memDc,0,0,SRCCOPY);
return bRet;
}


// WipDlg message handlers

void WipDlg::OnBnClickedButton3()
{

	
	/*Whoa, slow down doggy...*/
	if (AfxMessageBox(L"Archive ALL WIP scans? This process can only be reversed by a a sysadmin.",MB_YESNO,NULL) == IDNO) 
		return;

/*
1) Check to make sure we have a spot to archive to.
*/

	CString tempfloc;
	tempfloc = SETTINGS(WIPARCHIVE);
	tempfloc = GetFolderOnly(tempfloc);
	CFileStatus status;
	if (!CFile::GetStatus(tempfloc,status)) {
		AfxMessageBox(L"Invalid Archive Location. No Data uploaded. " +tempfloc, NULL, NULL);
		return;
	}
	LOG("Found Archive Loc");

/*
2) If we do, archive the old file.
*/
	
	tempfloc = SETTINGS(WIPSERVER);
	int archivestatus=1;

	if (CFile::GetStatus(tempfloc,status)) {
	archivestatus = ArchiveFile(SETTINGS(WIPSERVER), SETTINGS(WIPARCHIVE));
		//return values: 0 = No file to archive, 1 = Archive failed, 2 = Archive Success
	}
	switch(archivestatus) {
		case 0:
			AfxMessageBox(L"No File to Archive.",NULL,NULL);
			break;
		case 1:
			AfxMessageBox(L"Archive failed! Contact SysAdmin before uploading!",NULL,NULL);
			break;
		case 2:
			AfxMessageBox(L"WIP File Archived",NULL,NULL);
			DeleteFile(SETTINGS(WIPSERVER));
			break;
	}

}

void WipDlg::OnBnClickedButton1()
{

	CString lc;
	int lctoi;
	

	if (GetFileFromDevice(SETTINGS(WIPDEVICE),SETTINGS(PULLTMP),0) == 1)
	{
	AfxMessageBox(L"Error Reading Device.",NULL,NULL);
	return;
	}
	
	LOG("GetFileFromDevice(" + SETTINGS(WIPDEVICE) + SETTINGS(PULLTMP));

/*
4) Copy file to server
   If we fail, break.
*/

	
	//if (CopyFilePreApp(SETTINGS(PULLTMP),SETTINGS(WIPTEMP),SETTINGS(WIPPREPEND),SETTINGS(WIPAPPEND)) == false) {
	//	AfxMessageBox(L"Could not copy file to server.",NULL,NULL);
	//	return;
//	}
	
	/* Finally, if everything worked OK, delete the file from the Device.*/
	CeDeleteFile(SETTINGS(WIPDEVICE));
	ShellExecute(NULL,NULL,SETTINGS(STRINGCONVERTER),STRINGCARGS,NULL,SW_SHOW);
	AfxMessageBox(L"Wait for dialog box to finish...!",NULL,NULL);
	lctoi = openwipdb(SETTINGS(WIPTEMP),&DeviceWIPdb);
			openwipdb(SETTINGS(WIPSERVER),&ServerWIPdb);





/******************************MERGE DB's********************************************/
	int duplicatesfound = 0;
	int serverrecords = 0;
	int uploadedjobs = 0;
	bool foundit=false;

size_t i, j;
CString sizestr;
sizestr.Format(L" %d / %d",DeviceWIPdb.size(),lctoi);
LOG("Device Size:" + sizestr);

serverrecords = ServerWIPdb.size();


//AfxMessageBox(sizestr);


	for (j=0;j < this->DeviceWIPdb.size(); j++) {
		
		for (i=0; i < this->ServerWIPdb.size();i++)
				{
					//Check all server entries against current entry, then update server record if found.
					if (ServerWIPdb[i][WIP_JOB].CompareNoCase(DeviceWIPdb[j][WIP_JOB]) == 0) //&& wipdb[i][WIP_DEPT] == theApp.wipdepartment)
					{		
						ServerWIPdb[i][WIP_DEPT] = DeviceWIPdb[j][WIP_DEPT]; //new department, old job.
						ServerWIPdb[i][WIP_DATE] = DeviceWIPdb[j][WIP_DATE];
						ServerWIPdb[i][WIP_TIME] = DeviceWIPdb[j][WIP_TIME];	
						duplicatesfound++;
						foundit=true;
			
					}
				}
				
				//if we reach the end and didn't find it...
		if (foundit == false) {
			std::vector <CString> b;
						b.push_back(DeviceWIPdb[j][WIP_USER]);
						b.push_back(DeviceWIPdb[j][WIP_DEPT]);
						b.push_back(DeviceWIPdb[j][WIP_JOB]);
						b.push_back(DeviceWIPdb[j][WIP_DATE]); //date
						b.push_back(DeviceWIPdb[j][WIP_TIME]); //time
		
						this->ServerWIPdb.push_back(b);
						uploadedjobs++;
										
						
		}
				foundit=false;
				//Found it, so continue on to the next...
	}


/**************************************************************************/

LOG("Writing WIP DB");
writewipdb(SETTINGS(WIPSERVER),&ServerWIPdb);
DeviceWIPdb.clear();
ServerWIPdb.clear();

/*
	PrimaryWIPdb.clear();
	lc.Format(L"%d",lctoi);
	//ShellExecute(NULL,L"open",L"wiptmp.bat",NULL,NULL,SW_SHOW);
	LOG(" ShellExecute: " + SETTINGS(STRINGCONVERTER) + L"  " + STRINGCARGS );
	LOG("Get inv from " + SETTINGS(WIPDEVICE) + L" to " + SETTINGS(WIPPREPEND) + SETTINGS(WIPTEMP) + SETTINGS(WIPAPPEND) );
	AfxMessageBox(lc + L" Records uploaded.",NULL,NULL);
*/
CString reports;

reports.Format(L"%d",uploadedjobs);
SetDlgItemText(IDC_EDIT3,reports); //Jobs Uploaded
reports.Format(L"%d",duplicatesfound);
SetDlgItemText(IDC_EDIT4,reports); //duplicates
reports.Format(L"%d (%d + %d)",serverrecords+uploadedjobs,serverrecords,uploadedjobs);
SetDlgItemText(IDC_EDIT5,reports); //total server records

}



int WipDlg::openwipdb(CString wipdbfilename, std::vector< std::vector <CString> > * wipdb) {
	

		CString line;
		CFileException e;
		TCHAR strError[512] = _T("");
		LOG("Opening " + wipdbfilename);
		if (!wipdbfile.Open(wipdbfilename, CStdioFile::modeCreate | CStdioFile::shareDenyWrite |
			CStdioFile::modeNoTruncate | CFile::typeText, &e)) {
				e.GetErrorMessage(strError,512);
				AfxMessageBox(strError);
				return false;
		}
	
int index=0;
		
		//NOTE: This assumes a whole lot. any file changes will pooch this code.
		//To my future self: I'm sorry.

		while( wipdbfile.ReadString( line ) )   
		{

			line.TrimLeft();
			line.TrimRight();
			if (line.CompareNoCase(L"<WIP>") == 0) {
				index++;
				z.clear();
				
					wipdbfile.ReadString(line);
					line.TrimLeft(); line.TrimRight();
					line.Replace(L"<EmployeeNumber><![CDATA[",NULL);
					line.Replace(L"]]></EmployeeNumber>",NULL);
					z.push_back(line);	


					wipdbfile.ReadString(line);
					line.TrimLeft(); line.TrimRight();
					line.Replace(L"<Department><![CDATA[",NULL);
					line.Replace(L"]]></Department>",NULL);
					z.push_back(line);	

					wipdbfile.ReadString(line);
					line.TrimLeft(); line.TrimRight();
					line.Replace(L"<JobNumber><![CDATA[",NULL);
					line.Replace(L"]]></JobNumber>",NULL);
					z.push_back(line);	

					wipdbfile.ReadString(line);
					line.TrimLeft(); line.TrimRight();
					line.Replace(L"<ScanDate><![CDATA[",NULL);
					line.Replace(L"]]></ScanDate>",NULL);
					z.push_back(line);	

					wipdbfile.ReadString(line);
					line.TrimLeft(); line.TrimRight();
					line.Replace(L"<ScanTime><![CDATA[",NULL);
					line.Replace(L"]]></ScanTime>",NULL);
					z.push_back(line);	

					
					wipdb->push_back(z);
					LOG(".");
					wipdbfile.ReadString(line); // </WIP>	
				}

		}//readstring

		wipdbfile.Close();

		return index;

	}//openwipdb

int WipDlg::writewipdb(CString wipdbfilename,std::vector< std::vector<CString> > * wipdb) {
		CString line;
		CFileException e;
		TCHAR strError[512] = _T("");
		int index = 0;
		if (!wipdbfile.Open(wipdbfilename, CStdioFile::modeCreate | CFile::modeWrite | CFile::typeText, &e)) 
		{
				e.GetErrorMessage(strError,512);
				AfxMessageBox(strError);
				return false;
		}
		wipdbfile.WriteString(L"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
		wipdbfile.WriteString(L"<WIPINVENTORY>\n");

		std::vector < std::vector<CString> >::iterator it;
		std::vector<CString>::iterator its;

		for (it = wipdb->begin(); it != wipdb->end(); it++)
		{
			index++;
			its = (*it).begin();
			line = L"<WIP>\n"; wipdbfile.WriteString(line);
			line = L"<EmployeeNumber><![CDATA["+ *its + L"]]></EmployeeNumber>\n"; wipdbfile.WriteString(line); its++;
			line =     L"<Department><![CDATA["+ *its + L"]]></Department>\n"; wipdbfile.WriteString(line); its++;
			line =      L"<JobNumber><![CDATA[" + *its + L"]]></JobNumber>\n"; wipdbfile.WriteString(line); its++;
			line = L"<ScanDate><![CDATA[" + *its + L"]]></ScanDate>\n"; wipdbfile.WriteString(line); its++;
			line = L"<ScanTime><![CDATA[" + *its + L"]]></ScanTime>\n"; wipdbfile.WriteString(line); its++;
			line = L"</WIP>\n"; wipdbfile.WriteString(line);

			/*for(its = (*it).begin(); its < (*it).end(); its++)
			{
				
				line += *its; 
				if (its < (*it).end() -1) {
					line += '^';
				}
				
			}*/
			//wipdbfile.WriteString( line );
		}


	wipdbfile.WriteString(L"</WIPINVENTORY>\n");
	wipdbfile.Close();
	return index;
	}

void WipDlg::OnBnClickedButton2()
{
	
	//theApp.dwip.DoModal();	
	openwipdb(SETTINGS(WIPSERVER),&ServerWIPdb);
	CString x;
	x.Format(L"%d",ServerWIPdb.size());
	ServerWIPdb.clear();
	SetDlgItemText(IDC_EDIT5,x);
	x = "0";
	SetDlgItemText(IDC_EDIT4,x);
	SetDlgItemText(IDC_EDIT3,x);
}


void WipDlg::OnBnClickedButton5() //ADD SINGLE JOB
{

	CString Jobno,Deptno;
	bool foundit=false;
	int duplicatesfound=0;
	


	openwipdb(SETTINGS(WIPSERVER),&ServerWIPdb);
	int serverrecords = ServerWIPdb.size();

	GetDlgItemText(IDC_EDIT1,Jobno);
	GetDlgItemText(IDC_EDIT2,Deptno);

			for (size_t i=0; i < this->ServerWIPdb.size();i++)
				{
					//Check all server entries against current entry, then update server record if found.
					if (ServerWIPdb[i][WIP_JOB].CompareNoCase(Jobno) == 0) //&& wipdb[i][WIP_DEPT] == theApp.wipdepartment)
					{		
						ServerWIPdb[i][WIP_DEPT] = Deptno; //new department, old job.
						ServerWIPdb[i][WIP_DATE] = L"ManualEntry";
						ServerWIPdb[i][WIP_TIME] = L"ManualEntry";
						duplicatesfound++;
						foundit=true;
			
					}
				}
				
				//if we reach the end and didn't find it...
		if (foundit == false) {
			std::vector <CString> b;
						b.push_back(L"ManualEntry");
						b.push_back(Deptno);
						b.push_back(Jobno);
						b.push_back(L"ManualEntry"); //date
						b.push_back(L"ManualEntry"); //time
		
						this->ServerWIPdb.push_back(b);
						
										
						
		}
			
		CString reports;

	reports.Format(L"1");
	SetDlgItemText(IDC_EDIT3,reports); //Jobs Uploaded
	reports.Format(L"%d",duplicatesfound);
	SetDlgItemText(IDC_EDIT4,reports); //duplicates
	reports.Format(L"%d (%d + 1)",serverrecords+1,serverrecords);
	SetDlgItemText(IDC_EDIT5,reports); //total server records


//ADD JOB


/**************************************************************************/

LOG("Writing WIP DB");
writewipdb(SETTINGS(WIPSERVER),&ServerWIPdb);
ServerWIPdb.clear();

}

void WipDlg::OnBnClickedButton6() //Remove
{
	CString Jobno,Deptno;
	bool foundit=false;
	
	


	openwipdb(SETTINGS(WIPSERVER),&ServerWIPdb);
	int serverrecords = ServerWIPdb.size();

	GetDlgItemText(IDC_EDIT1,Jobno);
	

			for (size_t i=0; i < this->ServerWIPdb.size();i++)
				{
					//Check all server entries against current entry, then update server record if found.
					if (ServerWIPdb[i][WIP_JOB].CompareNoCase(Jobno) == 0) //&& wipdb[i][WIP_DEPT] == theApp.wipdepartment)
					{		
						ServerWIPdb.erase(ServerWIPdb.begin()+ i);
						
						foundit=true;
			
					}
				}
				
				//if we reach the end and didn't find it...
			
		CString reports;

		if (foundit==true) {
			reports.Format(L"-1");
		}
		else reports.Format(L"0");

	SetDlgItemText(IDC_EDIT3,reports); //Jobs Uploaded
	reports.Format(L"0");
	SetDlgItemText(IDC_EDIT4,reports); //duplicates
	
	if (foundit==true) {
		reports.Format(L"%d (%d - 1)",serverrecords-1,serverrecords);
	} 
	else reports.Format(L"%d (%d - 0)",serverrecords,serverrecords);

	SetDlgItemText(IDC_EDIT5,reports); //total server records


//ADD JOB


/**************************************************************************/

LOG("Writing WIP DB");
writewipdb(SETTINGS(WIPSERVER),&ServerWIPdb);
ServerWIPdb.clear();
}
