#pragma once
#include "atlconv.h"
#include "afx.h"
#include "afxwin.h"
#include <vector>
#include <iostream>
#include <sstream>


#define WIP_USER 0
#define WIP_DEPT 1
#define WIP_JOB 2
#define WIP_DATE 3
#define WIP_TIME 4


// WipDlg dialog

class WipDlg : public CDialog
{
	DECLARE_DYNAMIC(WipDlg)

public:
	WipDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~WipDlg();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	 int openwipdb(CString wipdbfilename, std::vector < std::vector<CString> > * wipdb);
	 int writewipdb(CString wipdbfilename, std::vector < std::vector<CString> > * wipdb);

// Dialog Data
	enum { IDD = IDD_WIP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();



	 CStdioFile wipdbfile;

	 CString s;
	 std::vector < std::vector<CString> > PrimaryWIPdb;  //obsolete
	 std::vector < std::vector<CString> > SecondaryWIPdb; //obsolete
	 std::vector < std::vector<CString> > DeviceWIPdb;
	 std::vector < std::vector<CString> > ServerWIPdb;
	 std::vector <CString> v;
	 std::vector <CString> z;

	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton6();
};


