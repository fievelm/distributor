#pragma once
#include "stdafx.h"
#include "logfile.h"
#include "Distributor.h"

CStdioFile logfile;


/*
*
*  Open Logfile
*
*/
bool openlog() {
	if (theApp.DSettings[LOGGING] == L"YES") {
		CFileException e;
		TCHAR strError[512] = _T("");
	
		if (!logfile.Open(L"log.txt", CStdioFile::modeCreate | CStdioFile::modeNoTruncate | CStdioFile::modeWrite | CStdioFile::shareDenyWrite | CFile::typeText, &e)) 
			{
				e.GetErrorMessage(strError,512);
				AfxMessageBox(strError);
				return false;
			}
		return true;
		}
	else
		return true;
}

/*
*
*  Log String to file
*
*/

bool logstring(CString line) {
	if (theApp.DSettings[LOGGING] == L"YES") {
		SYSTEMTIME LocalSystemTime;
		GetLocalTime(&LocalSystemTime);		
		CString s;	

		s.Format(L"[%02d,%02d,%02d %02d:%02d.%02d]", LocalSystemTime.wDay,LocalSystemTime.wMonth,LocalSystemTime.wYear,LocalSystemTime.wHour,LocalSystemTime.wMinute,LocalSystemTime.wSecond);
		logfile.SeekToEnd();
		logfile.WriteString(s + line + L"\n");
		return true;
		}
	else 
		return true;
}


/*
*
*  Close Log File
*
*/

bool closelog() {
	if (theApp.DSettings[LOGGING] == L"YES")
		logfile.Close();

return true;
}
