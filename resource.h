//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Distributor.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_DISTRIBUTOR_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDD_ORDERING                    129
#define IDB_BITMAP1                     130
#define IDI_ICON1                       131
#define IDI_DIST                        131
#define IDD_DIALOG2                     132
#define IDD_INVENTORY                   132
#define IDB_BITMAP2                     133
#define IDB_BITMAP3                     134
#define IDD_DIALOG3                     135
#define IDI_ICON2                       137
#define IDI_INVENTORY                   137
#define IDD_WIP                         138
#define IDI_ICON3                       142
#define IDI_ICON4                       145
#define IDI_ICON5                       146
#define IDI_CLIP                        146
#define IDD_DIALOG4                     147
#define IDC_BUTTON1                     1004
#define IDC_BUTTON2                     1005
#define IDC_BUTTON3                     1006
#define IDC_BUTTON4                     1007
#define IDC_BUTTON5                     1008
#define IDC_EDIT1                       1009
#define IDC_BUTTON8                     1009
#define IDC_BUTTON6                     1010
#define IDC_BUTTON7                     1011
#define IDC_BUTTON9                     1012
#define IDC_BUTTON10                    1013
#define IDC_BUTTON11                    1014
#define IDC_BUTTON12                    1015
#define IDC_BUTTON13                    1016
#define IDC_BUTTON14                    1017
#define IDC_BUTTON15                    1018
#define IDC_EDIT2                       1019
#define IDC_BUTTON16                    1020
#define IDC_SRVPARTS                    1022
#define IDC_MYPARTS                     1023
#define IDC_STARTDUAL                   1026
#define IDC_CNCTPRIM                    1027
#define IDC_u                           1028
#define IDC_U                           1028
#define IDC_UPPRIM                      1028
#define IDC_CNCTSEC                     1029
#define IDC_UPSEC                       1030
#define IDC_MERGE                       1031
#define IDC_UPLOAD                      1032
#define IDC_LOG                         1033
#define IDC_PRIMREC                     1034
#define IDC_SECREC                      1035
#define IDC_DUPES                       1036
#define IDC_EDIT3                       1037
#define IDC_EDIT4                       1038
#define IDC_EDIT5                       1039

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        148
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1040
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
